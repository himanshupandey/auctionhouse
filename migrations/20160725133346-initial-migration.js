'use strict';

module.exports = {
	up: function (queryInterface, Sequelize, done) {
		/*
		 Add altering commands here.
		 Return a promise to correctly handle asynchronicity.

		 Example:
		 return queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		queryInterface.createTable(
			'User',
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				createdAt: {
					type: Sequelize.DATE
				},
				userName: {
					type: Sequelize.STRING,
					unique: true
				},
				updatedAt: {
					type: Sequelize.DATE
				},
				coins: Sequelize.INTEGER
			}
		).then(function () {
			return queryInterface.createTable(
				'Item',
				{
					id: {
						type: Sequelize.INTEGER,
						primaryKey: true,
						autoIncrement: true
					},
					name: Sequelize.STRING,
					image: Sequelize.STRING,
					createdAt: {
						type: Sequelize.DATE
					},
					updatedAt: {
						type: Sequelize.DATE
					}
				}
			);
		}).then(function () {
			return queryInterface.createTable("Auction", {
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				itemId: {
					type: Sequelize.INTEGER,

					references: {
						// This is a reference to another model
						model: 'Item',

						// This is the column name of the referenced model
						key: 'id'
					}
				},
				userId: {
					type: Sequelize.INTEGER,

					references: {
						// This is a reference to another model
						model: 'User',

						// This is the column name of the referenced model
						key: 'id'
					}
				},
				quantity: Sequelize.INTEGER,
				minBid: Sequelize.INTEGER,
				completed: Sequelize.BOOLEAN,
				createdAt: {
					type: Sequelize.DATE
				},
				updatedAt: {
					type: Sequelize.DATE
				}
			});
		}).then(function () {
			return queryInterface.createTable(
				'AuctionBid',
				{
					id: {
						type: Sequelize.INTEGER,
						primaryKey: true,
						autoIncrement: true
					},
					auctionId: {
						type: Sequelize.INTEGER,

						references: {
							// This is a reference to another model
							model: 'Auction',

							// This is the column name of the referenced model
							key: 'id'
						}
					},
					bidderId: {
						type: Sequelize.INTEGER,

						references: {
							// This is a reference to another model
							model: 'User',

							// This is the column name of the referenced model
							key: 'id'
						}
					},
					value: Sequelize.INTEGER,
					createdAt: {
						type: Sequelize.DATE
					},
					updatedAt: {
						type: Sequelize.DATE
					}
				}
			);
		})
		// 	.then(function () {
		// 	return queryInterface.addIndex(
		// 		'AuctionBid',
		// 		['auctionId', 'bidderId'],
		// 		{
		// 			indexName: 'AuctionBidderUnique',
		// 			indicesType: 'UNIQUE'
		// 		}
		// 	);
		// })
			.then(function () {

				return queryInterface.createTable(
					'UserItem',
					{
						id: {
							type: Sequelize.INTEGER,
							primaryKey: true,
							autoIncrement: true
						},
						quantity: Sequelize.INTEGER,
						itemId: {
							type: Sequelize.INTEGER,

							references: {
								// This is a reference to another model
								model: 'Item',

								// This is the column name of the referenced model
								key: 'id',

								// This declares when to check the foreign key constraint. PostgreSQL only.
								deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
							}
						},
						userId: {
							type: Sequelize.INTEGER,

							references: {
								// This is a reference to another model
								model: 'User',

								// This is the column name of the referenced model
								key: 'id',

								// This declares when to check the foreign key constraint. PostgreSQL only.
								deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
							}
						},
						createdAt: {
							type: Sequelize.DATE
						},
						updatedAt: {
							type: Sequelize.DATE
						}
					}
				);
			})
			// 	.then(function () {
			// 	return queryInterface.addIndex(
			// 		'UserItem',
			// 		['itemId', 'userId'],
			// 		{
			// 			indexName: 'UserItemUnique',
			// 			indicesType: 'UNIQUE'
			// 		}
			// 	);
			// })
			.catch(function (error) {
				console.error('Error', error);
				return queryInterface.dropAllTables();
			}).done(function () {
			done();
		});
	},

	down: function (queryInterface, Sequelize, done) {

		return queryInterface.dropAllTables().done(function () {
			done();
		});

		/*
		 Add reverting commands here.
		 Return a promise to correctly handle asynchronicity.

		 Example:
		 return queryInterface.dropTable('users');
		 */
	}
};
