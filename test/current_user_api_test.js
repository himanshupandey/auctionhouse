var path = require('path');
global.__basedir = path.resolve(__dirname, '../');
global.getAuctionHouseModule = function (requirePath) {
	return require(path.join(global.__basedir, requirePath));
};
var DatabaseCleaner = require('database-cleaner');
var databaseCleaner = new DatabaseCleaner('postgresql');

var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Promise = require('promise');
var Umzug = require('umzug');
var currentUserApi = getAuctionHouseModule('/AuctionHouse/apis/currentUser');
var db = getAuctionHouseModule('/models/index');



// return databaseCleaner.clean(db.sequelize, function(){
// 	return Promise.resolve();
// });
// return db.sequelize.query("drop schema public cascade; create schema public;")
// 	return db.sequelize.query("CREATE OR REPLACE FUNCTION truncate_tables(username IN VARCHAR) RETURNS void AS $$\
// DECLARE \
//     statements CURSOR FOR \
//         SELECT tablename FROM pg_tables \
//         WHERE tableowner = username AND schemaname = 'public'; \
// BEGIN \
//     FOR stmt IN statements LOOP \
//         EXECUTE 'TRUNCATE TABLE ' || quote_ident(stmt.tablename) || ' CASCADE;'; \
//     END LOOP; \
// END; \
// $$ LANGUAGE plpgsql;SELECT truncate_tables('MYUSER');")
// beforeEach(function (done) {
	// return umzug.execute({
	// 	migrations: ['20160725133346-initial-migration.js'],
	// 	method: 'down'
	// }).then(function (migrations) {
	//
	// 	umzug.execute({
	// 		migrations: ['20160725133346'],
	// 		method: 'up'
	// 	}).then(function () {
	// 		return umzug.execute({
	// 			migrations: ['20160725133346'],
	// 			method: 'down'
	// 		});
	// 	});
	// });
	// var promise = new Promise(function (resolve, reject) {
	// databaseCleaner.clean(db.sequelize, done);
	// setTimeout(function () {
	// 	done();
	// }, 2000);
	// });
	// done();
	// return promise.then(function(){
	// 	return done();
	// }).catch(function(){
	// 	return done();
	// });
// });
describe('currentUserApi', function () {
	describe('currentUser', function () {
		it('should return  when the current user is when session is present', function (done) {
			var current = {message: 'dummy'};
			var reqObj = {
				originialUrl: "localhost:3000/",
				session: {currentUser: current},
			};
			currentUserApi.getCurrentUser(reqObj).then(function (currentUser) {
				assert.equal(currentUser.message, 'dummy');
				done();
			}).catch(function (error) {
				assert.fail(error);
				done();
			});
		});
		it('should throw an error if new userName is passed to login', function (done) {
			var reqObj = {
				originialUrl: "localhost:3000/",
				session: {}
			};
			currentUserApi.login(reqObj, 'newUser').then(function (currentUser) {
				assert.fail('Should have thrown error')
				done();
			}).catch(function (error) {
				assert.equal(error.reason, 'userDoesNotExist');
				done();
			});
		});

		it('should return new user if new userName is passed to register', function (done) {
			var current = {message: 'dummy'};
			var reqObj = {
				originialUrl: "localhost:3000/",
				session: {},
			};
			currentUserApi.register(reqObj, 'newUser').then(function (newUser) {
				assert.equal(newUser.userName, 'newUser');
			}).catch(function (error) {
				assert.fail(error);
			});

			done();
		});
		// it('should set him 30 bread, 18 carrots, 1 diamond if new userName is passed to register and items are fetched', function (done) {
		// 	var current = {message: 'dummy'};
		// 	var reqObj = {
		// 		originialUrl: "localhost:3000/",
		// 		session: {},
		// 	};
		// 	currentUserApi.register(reqObj, 'newUser1').then(function (newUser) {
		// 		// db.UserItem.findAll({where:{userName: 'newUser1'}, include: [{model: db.Item, as: 'item', foreignKey: 'itemId'}]}).then(function(userItems){
		// 		reqObj.session.currentUser = newUser;
		// 		currentUserApi.getCurrentItems(reqObj).then(function (userItems) {
		// 			assert.equal(userItems.length, 49);
		// 			assert.equal(userItems.filter(function (userItem) {
		// 				return userItem.item.name === 'Bread'
		// 			}).length, 30);
		// 			assert.equal(userItems.filter(function (userItem) {
		// 				return userItem.item.name === 'Carrots'
		// 			}).length, 18);
		// 			assert.equal(userItems.filter(function (userItem) {
		// 				return userItem.item.name === 'Diamonds'
		// 			}).length, 1);
		// 			done();
		// 		}).catch(function (error) {
		// 			assert.fail(error);
		// 			done();
		// 		});
		// 	}).catch(function (error) {
		// 		assert.fail(error);
		// 		done();
		// 	});
		// });
	});
});
