module.exports = function (grunt) { // configuration for grunt tasks (grunt wrapper function)
	var path = require('path');
	global.__basedir = path.resolve(__dirname);
	console.log(__basedir);
	global.getAuctionHouseModule = function (requirePath) {
		return require(path.join(global.__basedir, requirePath));
	};
	var jsFileList = [ // js file which are going to be part of application js

			'<%= dirs.app_dir %>/public/assets/bower_components/angular/angular.min.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/angular-resource/angular-resource.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/angular-messages/angular-messages.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/angular-cookies/angular-cookies.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/angular-websocket/dist/angular-websocket.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/angular-ui-router/release/angular-ui-router.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/angular-ui-router-title/angular-ui-router-title.js',
			'<%= dirs.app_dir %>/public/assets/js/libs/angularjs-bootstrap-custom/ui-bootstrap-custom-tpls-1.3.2.js',
			'<%= dirs.app_dir %>/public/assets/bower_components/underscore/underscore.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/app.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/configs/routes.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/services/models/UserInfoService.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/services/models/AuctionService.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/services/websocketSync.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/services/resources.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/controllers/LogInOrSignUpCtrl.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/controllers/StartAuctionCtrl.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/directives/Auction.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/directives/Inventory.js',
			'<%= dirs.app_dir %>/public/assets/js/application/angular/AuctionHouse/directives/Header.js'
		],
		lessFiles = [
			'<%= dirs.app_dir %>/public/assets/less/bootstrap/bootstrap.less',
			'<%= dirs.app_dir %>/public/assets/less/bootstrap/theme.less',
			'<%= dirs.app_dir %>/public/assets/less/style.less',
			'<%= dirs.app_dir %>/public/assets/less/backend-style.less'
		];

	grunt.initConfig({ // grunt app configurations
		dirs: {
			app_dir: path.join(__basedir, 'AuctionHouse'),
			base_dir: __basedir
		},
		jshint: { // app to check js errors and warnings
			files: [
				'<%= dirs.base_dir %>/Gruntfile.js',
				'<%= dirs.app_dir %>/public/assets/js/application/**/*.js'
			]
		},
		watch: { // app to watch change in following file and run specified task
			files: [
				'<%= dirs.base_dir %>/Gruntfile.js',
				'<%= dirs.app_dir %>/public/assets/js/application/**/*.js',
				'<%= dirs.app_dir %>/public/assets/css/**/*.css',
				'<%= dirs.app_dir %>/public/assets/less/**/*.less',
				'<%= dirs.app_dir %>/public/assets/images/**/*.*',
				'<%= dirs.app_dir %>/public/assets/fonts/**/*.*'
			],
			tasks: ['default']
		},
		less: { // app to compile and compress less files
			development: {  // target
				options: {
					paths: ['<%= dirs.app_dir %>/public/assets/less']
				},
				files: {
					"<%= dirs.app_dir %>/public/static/css/style.css": lessFiles
				}
			},
			production: {
				options: {
					compress: true,
					paths: ['<%= dirs.app_dir %>/public/assets/less']
				},
				files: {
					"<%= dirs.app_dir %>/public/static/css/style.css": lessFiles
				}
			}
		},
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: jsFileList,
				dest: '<%= dirs.app_dir %>/public/static/js/application.js'
			}
		},
		uglify: { // app for concatenate, mangle and compress js files
			options: {
				mangle: true,
				sourceMap: true
			},
			application: {
				files: {
					'<%= dirs.app_dir %>/public/static/js/application.js': jsFileList
				}
			}
		}
	});

	// load grunt tasks
	grunt.loadNpmTasks('grunt-contrib-jshint'); // run this task with `grunt jshint`
	grunt.loadNpmTasks('grunt-contrib-watch'); // run this task with `grunt watch`
	grunt.loadNpmTasks('grunt-contrib-less'); // run this task with `grunt less`
	grunt.loadNpmTasks('grunt-contrib-uglify'); // run this task with `grunt uglify`
	grunt.loadNpmTasks('grunt-contrib-concat'); // run this task with `grunt uglify`

	// register default task of grunt
	var asset_hasher = getAuctionHouseModule('tasks/asset_hasher.js');
	grunt.registerTask('hashify', asset_hasher.generateAssetHash(grunt)); // run this task with `grunt hashify`
	grunt.registerTask('hashify_style_url', function () {
		var callback = (getAuctionHouseModule('tasks/postfix_url_hash.js')).postfixUrlWithHash(grunt);
		callback.apply(this, arguments);
	}); // run this task with `grunt hashify_style_url`
	grunt.registerTask('default', ['less:development', 'concat', 'hashify', 'hashify_style_url']); // run this task with `grunt`
	grunt.registerTask('prod', ['less:production', 'uglify', 'hashify', 'hashify_style_url']); // run this task with `grunt`
};
