var WebSocketServer = require('ws').Server;
var currentAuctionApi = getAuctionHouseModule('AuctionHouse/apis/currentAuction');
var currentUserApi = getAuctionHouseModule('AuctionHouse/apis/currentUser');
var _ = require('underscore');
var Promise = require('promise');
var db = getAuctionHouseModule('models/index');

exports.auctionCommunication = function (ws, server, app) {

	var auction = null, queue = [], defaultTimeout = 90000, incrementalTimeout = 10000, startTime, currentTimeout, setTimeoutInstance;


	function startAuctionTimer(timeout) {
		if (setTimeoutInstance) {
			clearTimeout(setTimeoutInstance);
		}
		startTime = new Date();
		currentTimeout = timeout;
		sendData('auctionTime', {startTime: startTime.getTime(), endTime: startTime.getTime() + currentTimeout});
		setTimeoutInstance = setTimeout(function () {
			currentAuctionApi.closeCurrentAuction().then(function () {
				sendData({status: 400, reason: "noActiveAuction", message: "No Active Auction"});
				app.emit('closeAuction');
			});
		}, timeout);
	}


	function sendData(type, data) {
		if (type === 'currentAuction' && data.id && (!auction || auction.id !== data.id)) {
			auction = data;
			startAuctionTimer(defaultTimeout);
		}
		if (ws.readyState === 1) {
			if (Array.isArray(type)) {
				return ws.send(JSON.stringify(type));
			}
			else {
				return ws.send(JSON.stringify({type: type, data: data}));
			}
		}
		else {
			queue.push(arguments);
			return Promise.resolve(arguments);
		}
	}


	ws.on('open', function () {
		function sendPendingData() {
			if (ws.readyState === 1 && queue.length !== 0) {
				var queueElement = queue.shift();
				sendData.apply(this, queueElement).then(function () {
					sendPendingData();
				});
			}
		}
	});

	app.on('updateAuctionTime', function(){
		if(startTime) {
			sendData({
				type: 'auctionTime',
				data: {startTime: startTime.getTime(), endTime: startTime.getTime() + currentTimeout}
			});
		}
	});

	app.on('startAuction', function (auctionData) {
		startAuctionTimer(defaultTimeout);
		sendData([{type: 'currentAuction', data: auctionData},
			{type: 'auctionTime', data: {startTime: startTime.getTime(), endTime: startTime.getTime() + currentTimeout}}]);
	});


	app.on('placeBid', function () {
		var currentTime = new Date();
		if ((currentTimeout - (currentTime - startTime)) < (incrementalTimeout)) {
			startAuctionTimer(incrementalTimeout);
		}
	});

	currentAuctionApi.getCurrentAuction().then(function (auctionData) {
		sendData([{type: 'currentAuction', data: auctionData},
			{type: 'auctionTime', data: {startTime: startTime.getTime(), endTime: startTime.getTime() + currentTimeout}}]);

	}).catch(function (error) {
		sendData('currentAuction', error);
	});
};

exports.auctionBidCommunication = function (ws, server, app) {
	var queue = [];

	function sendData(data) {
		if (ws.readyState === 1) {
			return ws.send(JSON.stringify({type: 'currentAuctionBid', data: data}));
		}
		else {
			queue.push(arguments);
			return Promise.resolve(arguments);
		}
	}

	ws.on('open', function () {
		function sendPendingData() {
			if (ws.readyState === 1 && queue.length !== 0) {
				var queueElement = queue.shift();
				sendData.apply(this, queueElement).then(function () {
					sendPendingData();
				});
			}
		}
	});
	app.on('placeBid', function (auctionBid) {
		sendData(auctionBid);
		app.emit('updateAuctionTime');
	});
	currentAuctionApi.getMaxBid().then(function (auctionBid) {
		sendData(auctionBid);

	}).catch(function (error) {
		sendData(error);
	});
};

exports.currentUserCommunication = function (ws, server, app, sessionMiddleWare) {
	var queue = [];

	function sendCurrentUserData() {

		var fakeRequest = _.extend(ws.upgradeReq, {originalUrl: ws.upgradeReq.url}), fakeResponse = {}, next = function () {
			if (fakeRequest.session.currentUser) {
				db.User.findOne({where: {id: fakeRequest.session.currentUser.id}}).then(function (user) {
					fakeRequest.session.currentUser = user;
					return Promise.resolve({});
				}).then(function () {
					return currentUserApi.getCurrentItems(fakeRequest).then(function (userItems) {
						return sendData([
							{type: 'currentUserItems', data: userItems},
							{type: 'currentUser', data: fakeRequest.session.currentUser}
						]);
					});
				});
			}
		};
		sessionMiddleWare(fakeRequest, fakeResponse, next)
	}

	function sendData(type, data) {
		if (ws.readyState === 1) {
			if (Array.isArray(type)) {
				return ws.send(JSON.stringify(type));
			}
			else {
				return ws.send(JSON.stringify({type: type, data: data}));
			}
		}
		else {
			queue.push(arguments);
			return Promise.resolve(arguments);
		}
	}

	ws.on('open', function () {
		function sendPendingData() {
			if (ws.readyState === 1 && queue.length !== 0) {
				var queueElement = queue.shift();
				sendData.apply(this, queueElement).then(function () {
					sendPendingData();
				});
			}
		}
	});
	app.on('closeAuction', function () {
		sendCurrentUserData();
	});
	sendCurrentUserData();
};

exports.registerWebSocketCommunication = function (server, app, sessionMiddleWare) {
	var wss = new WebSocketServer({server: server, path: '/api/v1/data-sync'});
	wss.on('connection', function connection(ws) {
		exports.currentUserCommunication(ws, server, app, sessionMiddleWare);
		exports.auctionCommunication(ws, server, app);
		exports.auctionBidCommunication(ws, server, app);
	});
};

