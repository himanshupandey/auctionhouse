module.exports = {
	defaultCoins: 1000,
	defaultBreads: 30,
	defaultCarrots: 18,
	defaultDiamonds: 1,
	breadImage: "http://lorempixel.com/100/98",
	carrotImage: "http://lorempixel.com/100/99",
	diamondImage: "http://lorempixel.com/100/100"
};
