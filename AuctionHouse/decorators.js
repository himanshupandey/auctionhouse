exports.loginRequired = function (req, res, next) {
    if (req && req.session && req.session.currentUser) {
    	next();
		}
		else{
			res.status(403);
			if(req.xhr){
				res.send({reason: 'loggedOut', message: "User is logged out"})
			}
			else{
				res.redirect('/login')
			}
		}
};
