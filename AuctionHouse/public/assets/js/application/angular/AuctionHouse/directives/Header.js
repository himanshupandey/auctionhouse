angular.module('AuctionHouseApp').directive('ahHeader', ["websocketSync",
	function (websocketSync) {

		return {
			restrict: 'E',
			templateUrl: 'template/common/_header',
			controller: function () {
				var self = this;
				self.currentUser = websocketSync.data.currentUser;
			},
			controllerAs: 'header'
		};
	}
]);
