angular.module('AuctionHouseApp').directive('auction', ["websocketSync", 'AuctionService', '$interval',
	function (websocketSync, AuctionService, $interval) {

		return {
			restrict: 'E',
			templateUrl: 'template/dashboard/partials/auction',
			controller: function () {
				var self = this;
				self.formData = {};
				self.minimumBidValue = 0;
				// self.currentUser = websocketSync.data.currentUser;
				// self.currentAuction = websocketSync.data.currentAuction;
				// self.auctionTime = websocketSync.data.auctionTime;
				// self.maxBid = websocketSync.data.currentAuctionBid;
				self.syncData = websocketSync.data;

				self.timeRemaining = null;
				self.updateTimeRemaining = function(){
					var intervalInstance = $interval(function(){
						self.timeRemaining = Math.round((self.syncData.auctionTime.endTime - self.syncData.auctionTime.startTime) - (self.syncData.auctionTime.endtime - (new Date()).getTime()) / 1000);
						if(self.timeRemaining === 0){
							$interval.cancel(intervalInstance);
						}
					}, 1000);
				};

				var updateBidValue = function(){
					if((self.currentAuction &&self.syncData.currentAuction.id || self.syncData.currentAuctionBid.value)&& (!self.formData.bidValue || self.formData.bidValue <= (self.syncData.currentAuctionBid.value ||self.syncData.currentAuction.minBid))){
						self.formData.bidValue = (self.syncData.currentAuctionBid.value ||self.syncData.currentAuction.minBid) + (self.syncData.currentAuctionBid.value? 1 : 0);
					}
					self.minimumBidValue = (self.syncData.currentAuctionBid.value ||self.syncData.currentAuction.minBid) + (self.syncData.currentAuctionBid.value? 1 : 0);
					if(self.currentAuction && self.syncData.currentAuction.id){
						self.updateTimeRemaining();
					}
				};
				websocketSync.setCallback('auctionUpdateBidValue', updateBidValue);
				updateBidValue();

				self.clearServerError = function () {
					self.serverError = {
						activeAuctionPresent: false,
						internalError: false
					};
				};

				self.placeBid = function (form) {
					form.$setSubmitted();
					if (form.$valid) {
						AuctionService.placeBid({
							bidValue: self.formData.bidValue
						}).then(function (auction) {
						}).catch(function (error) {
							if (error.reason === 'noActiveAuction') {
								self.serverError.noActiveAuction = true;
							}
							else {
								self.serverError.interError = true;
							}
						});
					}
				};
			},
			controllerAs: 'auction'
		};
	}
]);
