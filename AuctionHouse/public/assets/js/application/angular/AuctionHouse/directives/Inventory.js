angular.module('AuctionHouseApp').directive('inventory', ['$uibModal', 'websocketSync',
	function ($uibModal, websocketSync) {

		return {
			restrict: 'E',
			templateUrl: 'template/dashboard/partials/inventory',
			controller: function ($uibModal) {
				var self = this;
				self.currentUserItems = websocketSync.data.currentUserItems;

				/***
				 * [Function to initiate creation of conflict. It opens up a modal with form to get details related to conflict]
				 * @param userItem [application for conflict is to be created]
				 */
				self.startAuction = function (userItem) {
					var modalInstance = $uibModal.open({
						animation: true,
						size: 'md',
						windowTemplateUrl: '/template/dashboard/partials/modals/generic-modal-window',
						templateUrl: '/template/dashboard/partials/modals/start-auction-modal',
						controller: 'StartAuctionCtrl',
						controllerAs: "startAuctionCtrl",
						resolve: {
							options: function () {
								return {
									userItem: userItem
								};
							}
						}
					});
				};
			},
			controllerAs: 'inventory'
		};
	}
]);

