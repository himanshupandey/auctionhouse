angular.module("AuctionHouseApp").factory('websocketSync', ['$websocket', '$location', '$interval', function ($websocket, $location, $interval) {
	// Open a WebSocket connection
	var port = $location.port(), auctionUrl;
	if(port){
		auctionUrl = 'ws://' + $location.host() + ':' + port + '/api/v1/data-sync';
	}
	else{
		auctionUrl = 'ws://' + $location.host() + '/api/v1/data-sync';
	}

	var typeHandler = {
		currentUser: function (dataObj) {
			if (dataObj.id) {
				angular.copy(dataObj, data.currentUser);
			}
			else{
				angular.copy({}, data.currentUser);
			}
		},
		currentUserItems: function (dataObj) {
			if (Array.isArray(dataObj)) {
				angular.copy(dataObj, data.currentUserItems);
			}
			else{
				angular.copy([], data.currentUserItems);
			}
		},
		currentAuction: function (dataObj) {
			if (dataObj.id) {
				angular.copy(dataObj, data.currentAuction);
			}
			else{
				angular.copy({}, data.currentAuction);
			}
		},
		currentAuctionBid: function (dataObj) {
			if (dataObj.id) {
				angular.copy(dataObj, data.currentAuctionBid);
			}
			else{
				angular.copy({}, data.currentAuctionBid);
			}
		},
		auctionTime: function(dataObj){
			if (dataObj.startTime && dataObj.endTime) {
				angular.copy(dataObj, data.auctionTime);
			}
			else{
				angular.copy({}, data.auctionTime);
			}
		},
		defaultType: function (dataObj) {}
	};

	var data = {
		currentUser: {},
		currentUserItems: [],
		currentAuction: {},
		currentAuctionBid: {}
	};
	var callback = {};
	var timerInstance = 0;
	function setCallback(name, callbackFunction){
		callback[name] = callbackFunction;
	}

	function destroyCallback(name) {
		if(callback[name] !== undefined) {
			delete callback[name];
		}
	}

	var dataStream;
	function init() {
		dataStream = $websocket(auctionUrl);
		dataStream.onMessage(function (message) {
			var receivedData = JSON.parse(message.data);
			if(!Array.isArray(receivedData)) {
				if (typeof typeHandler[receivedData.type] === "function") {
					typeHandler[receivedData.type](receivedData.data);
				}
			}
			else{
				receivedData.forEach(function(data){
					if (typeof typeHandler[data.type] === "function") {
					typeHandler[data.type](data.data);
				}
				});
			}
			for(var name in callback){
				if(typeof callback[name] === 'function'){
					callback[name](receivedData);
				}
			}
		});
		dataStream.onClose(function () {
			if (!timerInstance) { /* avoid firing a new setInterval, after one has been done */
				timerInstance = $interval(function () {
					init();
				}, 5000);
			}
		});
		dataStream.onOpen(function (){
			if(timerInstance) {
				$interval.cancel(timerInstance)
			}
		});
	}
	init();

	var methods = {
		data: data,
		setCallback: setCallback,
		destroyCallback: destroyCallback
	};

	return methods;
}]);
