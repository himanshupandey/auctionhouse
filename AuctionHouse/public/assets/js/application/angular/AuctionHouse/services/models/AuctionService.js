/**
 * AuctionService service contains methods for interaction with auction
 */
angular.module("AuctionHouseApp").factory("AuctionService", ["AuctionApi", "$q", function(AuctionApi, $q) {

  var AuctionService = {};

  AuctionService.getCurrentAuction = function(params){
    return AuctionApi.currentAuction(params).$promise.then(function(auction){
      return auction
    }).catch(function(error){
      if(error && error.data && error.data.reason === 'noActiveAuction') {
        return {};
      }
      else{
        return $q.reject(error);
      }

    });
  };
  AuctionService.startAuction = function(params){
    return AuctionApi.startAuction(params).$promise.then(function(auction){
      return auction
    }).catch(function(error){
      return $q.reject(error);
    });
  };
  AuctionService.placeBid = function(params){
    return AuctionApi.placeBid(params).$promise.then(function(auctionBid){
      return auctionBid;
    }).catch(function(error){
      return $q.reject(error);
    });
  };
  return AuctionService;
}]);
