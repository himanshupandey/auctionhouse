/**
 * Userinfo service contains methods relting to a logged in user eg. user id, type, agency,manager etc.
 */
angular.module("AuctionHouseApp").factory("UserInfoService", ["UserApi", '$q', function(UserApi, $q) {

  var UserInfoService = {};

  var userDetails = null,
    userItems = null;
  UserInfoService.loginOrRegister = function(userName){
    return UserApi.loginOrRegister({userName: userName}).$promise.then(function(user){
      userDetails = user;
      return user;
    });
  };
  UserInfoService.getCurrentUser = function(){
    if(userDetails){
      return $q.when(userDetails);
    }
    return UserApi.currentUser().$promise.then(function(user){
      userDetails = user;
      return user;
    });
  };
  UserInfoService.getCurrentUserItems = function(){
    if(userItems){
      return $q.when(userItems);
    }
    return UserApi.currentUserItems().$promise.then(function(items){
      userItems = items;
      return items;
    });
  };
  return UserInfoService;
}]);
