/*--------------------------Resources for employer module------------------------------*/
angular.module("AuctionHouseApp")
  /**
   * [Resource to get logged in organization's details]
   * @param  {[Object]} $resource)
   * @return {[Object]}
   */
  .factory("UserApi", ["$resource", function($resource) {
    return $resource('api/v1/', {
    }, {
      'loginOrRegister': {
        url: "api/v1/login-or-register",
        method: 'POST',
        isArray: false,
      },
      'currentUser': {
        url: "api/v1/current-user",
        method: 'GET',
        isArray: false
      },
      'currentUserItems': {
        url: "api/v1/current-user-items",
        method: 'GET',
        isArray: true
      }
    });
  }])
  .factory("AuctionApi", ["$resource", function($resource) {
    return $resource('api/v1/', {
    }, {
      'currentAuction': {
        url: "api/v1/current-auction",
        method: 'GET',
        isArray: false
      },
      'startAuction': {
        url: "api/v1/start-auction",
        method: 'POST',
        isArray: false
      },
      'placeBid': {
        url: "api/v1/place-bid",
        method: 'POST',
        isArray: false
      }
    });
  }]);
