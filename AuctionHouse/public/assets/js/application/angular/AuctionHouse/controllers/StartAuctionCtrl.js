angular.module('AuctionHouseApp').controller('StartAuctionCtrl', ["AuctionService", "$uibModalInstance", "options",
	function (AuctionService, $uibModalInstance, options) {
		var self = this;
		self.userItem = options.userItem;
		self.formData = {};
		self.serverError = {
			activeAuctionPresent: false,
			internalError: false
		};
		self.clearServerError = function() {
			self.serverError = {
				activeAuctionPresent: false,
				internalError: false
			};
		};
		self.dismiss = function(){
			$uibModalInstance.dismiss('cancel');
		};
		self.submitForm = function (form) {
			form.$setSubmitted();
			if (form.$valid) {
				AuctionService.startAuction({
					minBid: self.formData.minBid,
					itemId: self.userItem.item.id,
					quantity: self.formData.quantity
				}).then(function (auction) {
					$uibModalInstance.close(auction);
				}).catch(function (error) {
					if(error.reason === 'activeAuctionPresent'){
						self.serverError.activeAuctionPresent = true;
					}
					else{
						self.serverError.internalError = true;
					}
				});
			}
		};
	}
]);
