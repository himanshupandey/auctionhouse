angular.module('AuctionHouseApp').controller('LogInOrSignUpCtrl', ["UserInfoService", "$state",
  function(UserInfoService, $state) {
		var self = this;
		self.formData = {};

		self.submitForm = function(form){
			form.$setSubmitted();
			if(form.$valid){
				UserInfoService.loginOrRegister(self.formData.userName).then(function(user){
					$state.go('dashboard');
					}).catch(function(){
						form.userName.$setValidity('internalError', false);
				});
			}
		};
	}
]);
