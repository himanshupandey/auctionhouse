angular.module('AuctionHouseApp', ["ngCookies", "ngResource", "ui.router",'ui.router.title', 'ngMessages','ui.bootstrap', 'angular-websocket']);
angular.module('AuctionHouseApp').run([function(){}]);
angular.module('AuctionHouseApp').config(["$resourceProvider", function($resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false; //To stop striping of trailing slashes during $http calls
}]);
// angular.module('AuctionHouseApp').config(["$httpProvider", function($httpProvider) {
//   $httpProvider.interceptors.push('authInterceptor');
// }]);
