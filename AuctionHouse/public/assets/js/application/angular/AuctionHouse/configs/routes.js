angular.module("AuctionHouseApp").config(['$stateProvider', '$urlRouterProvider', '$locationProvider', "$urlMatcherFactoryProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {
	//locationProvider settings
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: true
	});

	//If invalid/abstract url,
	// redirect to "home"
	// $urlRouterProvider.otherwise("/dashboard/auction");

	//Making trailing slashes in the url optional
	$urlMatcherFactoryProvider.strictMode(false);

	// Now set up the states
	$stateProvider

		.state('dashboard', {
			url: "/",
			controller: 'DashboardCtrl',
			views: {
				"main": {
					templateUrl: "template/main"
				},
				"header": {
					templateUrl: "template/common/_header",
					controller: 'HeaderCtrl',
					controllerAs: "headerCtrl"
				}
			},
			// views: {
			// 	"header": {
			// 		templateUrl: "template/common/_header",
			// 		controller: 'HeaderCtrl',
			// 		controllerAs: "headerCtrl"
			// 	},
			// 	"main": {
			// 		templateUrl: "template/main"
			// 	},
			// 	"inventory@dashboard": {
			// 		templateUrl: "template/dashboard/partials/inventory",
			// 		controller: 'InventoryCtrl',
			// 		controllerAs: "inventoryCtrl"
			// 	},
			// 	"auction@dashboard": {
			// 		templateUrl: "template/dashboard/partials/auction",
			// 		controller: 'AuctionCtrl',
			// 		controllerAs: "auctionCtrl"
			// 	}
			// },
			resolve: {
			// 	currentAuction: ["AuctionService", function(AuctionService){
			// 		return AuctionService.getCurrentAuction();
			// 	}],
				currentUser: ["UserInfoService", function(UserInfoService){
					return UserInfoService.getCurrentUser();
				}]
			// 	currentUserItems: ["UserInfoService", function(UserInfoService){
			// 		return UserInfoService.getCurrentUserItems();
			// 	}]
			}
		})

		.state('loginOrSignUp', {
			url: "/login",
			views: {
				"main": {
					templateUrl: "template/sign_in",
					controller: "LogInOrSignUpCtrl",
					controllerAs: "loginOrRegisterFormCtrl"
				}
			}
		});
}]);
