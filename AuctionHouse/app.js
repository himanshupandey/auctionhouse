var path = require('path');
global.__basedir = path.resolve(__dirname, '../');
global.getAuctionHouseModule = function (requirePath) {
	return require(path.join(global.__basedir, requirePath));
};

var express = require('express');
var url = require('url')
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressLayouts = require('express-ejs-layouts');
var WebSocketServer = require('ws').Server;
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var config = require('config');
var env = config.get('PRODUCTION') ? 'production' : 'development';

var webApp = getAuctionHouseModule('AuctionHouse/webApp/routes');
var apiRoutes = getAuctionHouseModule('AuctionHouse/apis/routes');
var websocketApi = getAuctionHouseModule('AuctionHouse/websocketSyncApi');
var redisMemoryStore = new RedisStore({
	host: config.get('redisSetting.host'),
	port: config.get('redisSetting.port'),
	prefix: config.get('redisSetting.prefix'),
	expire: 86400
});
var app = express();

// expressWs = require('express-ws')(app);
// app = expressWs.app;

var redis = require('redis');
var client = redis.createClient(config.get('redisSetting.port'), config.get('redisSetting.host'));

var sessionMiddleware = session({
	store: redisMemoryStore,
	cookie: {maxAge: 2628000000},
	secret: config.get('session_secret'),
	proxy: false,
	resave: true,
	saveUninitialized: true
});

app.use(sessionMiddleware);
var assetHelper = (getAuctionHouseModule('asset_helper.js')).asset_helper();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);

assetHelper.registerAssetHelper(app, env); // adding helper for app

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(function (req, res, next) {
	res.locals = res.locals || {};
	res.locals.baseUrl = config.get('baseUrl');
	next();
});
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.registerWebSocketCallbacks = function (server) {
	websocketApi.registerWebSocketCommunication(server, app, sessionMiddleware);
};
// set app routes
app.use('/', webApp);
app.use('/api/v1/', apiRoutes);


//serve template files
app.use(function (req, res, next) {
	var templateUrlRegex = new RegExp("\/template\/(.*)$");
	var match = templateUrlRegex.exec(req.originalUrl);
	if (match) {
		var fileName = match[1];
		try {
			res.render(fileName, {layout: 'layout_partial'});
		}
		catch (error) {
			next(error);
		}
	}
	else {
		next();
	}
});

app.use(express.static('AuctionHouse/public'));


//
// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// error handlers

// development error handler
// will print stacktrace
if (config.get('PRODUCTION') === false) {
	app.use(function (err, req, res, next) {
		var templateToRender = 'common/500.ejs';
		console.error(err);
		res.status(err.status || 500);
		if (res.status) templateToRender = 'common/404.ejs';
		res.render(templateToRender, {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('common/500.ejs', {
		message: err.message,
		error: {}
	});
});


module.exports = app;
