var express = require('express');
var router = express.Router();
var currentUserApi = getAuctionHouseModule('AuctionHouse/apis/currentUser');

/* GET home page. */
router.get('/', function (req, res, next) {
	if (req && req.session && req.session.currentUser) {
		res.render('index.ejs', {title: 'Auction House'});
	}
	else {
		res.redirect("/login");
	}
});

router.get('/login', function (req, res, next) {
	res.render('index.ejs', {title: 'Auction House'});
});

router.get('/logout', function (req, res, next) {
	currentUserApi.logout(req).then(function () {
		res.redirect('/login');
	}).catch(function (error) {
		console.error(error);
		res.render('common/500.ejs');
	});
});

module.exports = router;
