"use strict";
var Promise = require('promise');
var db = getAuctionHouseModule('models/index');
// Create REST resource
var auctionApis = {
	_currrentAuction: null,
	getCurrentAuction: function () {
		return db.Auction.findOne({
			order: [
				['createdAt', 'DESC']
			],
			where: {
				completed: {
					$or: {
						$in: [null, false]
					}
				}
			},
			include: [
				{model: db.Item, as: 'item', foreignKey: 'itemId'},
				{model: db.User, as: 'user', foreignKey: 'userId'}
			]
		}).then(function (auction) {
			auctionApis._currrentAuction = auction;
			if (auction) {
				return auction;
			}
			else {
				return Promise.reject({status: 400, reason: "noActiveAuction", message: "No Active Auction"})
			}
		}).catch(function (obj) {
			console.error(obj);
			return Promise.reject({
				status: obj.status || 400,
				reason: obj.reason || "internalError",
				message: obj.message || JSON.stringify(obj)
			});
		});
	},
	startAuction: function (req, params) {
		if (req && req.session && req.session.currentUser) {
			// validation
			if (!params.itemId) return Promise.reject({reason: 'itemIdRequired', message: "Item Id is required"});
			if (typeof params.quantity !== 'number') return Promise.reject({
				reason: 'quantityRequired',
				message: "Quantity is required"
			});
			if (typeof params.minBid !== 'number') return Promise.reject({
				reason: 'minBidRequired',
				message: "Minimum Bid is required"
			});
			if (params.quantity <= 0) return Promise.reject({reason: 'invalidQuantity', message: "Invalid Quantity"});
			if (params.minBid < 0) return Promise.reject({reason: 'invalidBid', message: "Invalid Bid"});

			return db.UserItem.findOne({where: {id: params.itemId}}).then(function (userItem) {
				if (userItem.quantity < params.quantity) {
					return Promise.reject({reason: 'quantityExceeded', message: 'Quantity should be less than the item present'});
				}
				else {
					return auctionApis.getCurrentAuction();
				}
			}).then(function (auction) {

				return Promise.reject({reason: 'activeAuctionPresent', message: "There is already a auction going on"});
			}).catch(function (error) {
				if (error && error.reason === 'noActiveAuction') {
					return db.Auction.create({
						userId: req.session.currentUser.id,
						quantity: params.quantity,
						itemId: params.itemId,
						minBid: params.minBid,
						completed: false
					}).then(function (auction) {
						return db.Auction.findOne({
							where: {id: auction.id},
							include: [
								{model: db.Item, as: 'item', foreignKey: 'itemId'},
								{model: db.User, as: 'user', foreignKey: 'userId'}
							]
						});
					}).then(function(auction){
						auctionApis._currrentAuction = auction;
						if (req.app) {
							req.app.emit('startAuction', auction);
						}
						return auction;
					});
				}
				else {
					return Promise.reject(error);
				}
			});
		}
		else {
			return Promise.reject({reason: 'loggedOut', message: "User is logged out"});
		}
	},
	getMaxBid: function () {
		return auctionApis.getCurrentAuction().then(function (obj) {
			// return db.AuctionBid.upsert({auctionId: obj.id, bidderId: user.id, value: value});
			return db.AuctionBid.findOne({
				order: [
					['value', 'DESC']
				],
				where: {
					value: {
						$or: {
							$ne: null
						}
					}
				},
				include: [
					{model: db.Auction, as: 'auction', foreignKey: 'auctionId'},
					{model: db.User, as: 'bidder', foreignKey: 'bidderId'}
				]
			}).then(function (auctionBid) {
				if (auctionBid) {
					return auctionBid;
				}
				else {
					return Promise.reject({status: 400, reason: "noBid", message: "No Bid"})
				}
			}).catch(function (obj) {
				console.error(obj);
				return Promise.reject({
					status: obj.status || 500,
					reason: obj.reason || "internalError",
					message: obj.message || JSON.stringify(obj)
				});
			});

		});
	},
	placeBid: function (req, params) {
		var auctionBid;
		if (req && req.session && req.session.currentUser) {
			// validation
			if (typeof params.bidValue !== 'number') return Promise.reject({
				reason: 'bidValueRequired',
				message: "Bid Value is required"
			});
			if (params.bidValue < 0) return Promise.reject({reason: 'invalidBid', message: "Invalid Bid"});
			return auctionApis.getMaxBid(
			).then(function (auctionBidObj) {
				if (params.bidValue < auctionBidObj.value && params.bidValue > req.session.currentUser.coins && req.session.currentUser.id === auctionBidObj.auction.userId) {
					return Promise.reject({reason: 'invalidBid', message: "Invalid Bid"});
				}
				return auctionBidObj;
			}).catch(function (error) {
				if (error && error.reason === 'noBid') {
					return Promise.resolve({reason: 'noActiveBid'});
				}
				else {
					return Promise.reject(error);
				}
			})
				.then(function () {
					return db.AuctionBid.findOne({where: {auctionId: auctionApis._currrentAuction.id, bidderId: req.session.currentUser.id}});
				}).then(function (auctionBidObj) {
					if (!auctionBidObj) {
						return db.AuctionBid.create({
							bidderId: req.session.currentUser.id,
							value: params.bidValue,
							auctionId: auctionApis._currrentAuction.id
						});
					}
					else {
						auctionBidObj.value = params.bidValue;
						return auctionBidObj.save();
					}
				}).then(function (auctionBidUpdated) {
					if (req.app) {
						req.app.emit('placeBid', auctionBidUpdated);
					}
					return auctionBidUpdated;
				}).catch(function (obj) {
					console.error(obj);
					return Promise.reject({
						status: obj.status || 500,
						reason: obj.reason || "internalError",
						message: obj.message || JSON.stringify(obj)
					});
				});
		}
		else {
			return Promise.reject({reason: 'loggedOut', message: "User is logged out"});
		}
	},
	closeCurrentAuction: function (app) {
		console.log(1);
		var currentBidder, currentAuction, maxBid, userItems;
		currentAuction = auctionApis._currrentAuction;
		return auctionApis.getMaxBid()
			.catch(function (error) {
				console.log(2);
				if (error.reason === 'noBid') {
					console.log(3);
					return Promise.resolve({reason: 'noBidExist'})
				}
				else {
					console.log(4);
					return Promise.reject(error)
				}
			})
			.then(function (auctionBid) {
				console.log(5);
				maxBid = auctionBid;
				console.log('read');
				if (!currentAuction.completed && (!maxBid || maxBid.reason === 'noBidExist')) {
					console.log(6);
					currentAuction.completed = true;
					return currentAuction.save();
				}
				else if(!currentAuction.completed){
					console.log(7);
					currentBidder = auctionBid.bidder;
					return db.UserItem.findAll({
						where: {
							userId: {
								$in: [auctionBid.bidderId, currentAuction.userId]
							},
							itemId: currentAuction.itemId
						}
					}).then(function (data) {
						console.log(8);
						userItems = data;
						return db.sequelize.transaction({
							isolationLevel: db.Sequelize.Transaction.ISOLATION_LEVELS.SERIALIZABLE
						}, function (transaction) {
							console.log('write');
							var userItems = data;
							var bidValue = maxBid.value;
							currentAuction = auctionApis._currrentAuction;
							currentAuction.user.coins += bidValue;
							currentBidder.coins -= bidValue;
							currentAuction.completed = true;
							var promises = [currentAuction.save({transaction: transaction}),
								currentAuction.user.save({transaction: transaction}),
								currentBidder.save({transaction: transaction})];
							promises = promises.concat([
								[currentBidder.id, auctionApis._currrentAuction.quantity],
								[auctionApis._currrentAuction.userId, -auctionApis._currrentAuction.quantity]
							].map(function (itemData) {
								var userItemsMatched = userItems.filter(function (userItem) {
									return userItem.userId === itemData[0];
								});
								if (userItemsMatched.length === 0) {
									return db.UserItem.create({
										userId: itemData[0],
										itemId: auctionApis._currrentAuction.itemId,
										quantity: itemData[1]
									}, {transaction: transaction});
								}
								else {
									userItemsMatched[0].quantity += itemData[1];
									return userItemsMatched[0].save({transaction: transaction});
								}
							}));
							return Promise.all(promises);
							console.log(9);
						});
						console.log(19);
					});
					console.log(20);
				}
			}).then(function () {
				console.log(21);
				return auctionApis._currrentAuction;
			}).catch(function (error) {
				console.log(22);
				console.error('error', JSON.stringify(error));
				return Promise.reject({reason: 'internalError', message: JSON.stringify(error)});
				console.log(23);
			});
	}
};

module.exports = auctionApis;
