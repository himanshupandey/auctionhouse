"use strict";
var Promise = require('promise');
var db = getAuctionHouseModule('models/index');
var constants = getAuctionHouseModule('AuctionHouse/constants');
var _ = require('underscore');
// Create REST resource

var currentUserApi = {
	_currentParams: null,
	getCurrentUser: function (req) {
		if (req && req.session && req.session.currentUser) {
			return Promise.resolve(req.session.currentUser);
		}
		else {
			return Promise.reject({reason: 'loggedOut', message: "User is logged out"});
		}
	},
	getCurrentItems: function (req) {
		if (req && req.session && req.session.currentUser) {
			return db.UserItem.findAll({where:{userId: req.session.currentUser.id}, include: [{model: db.Item, as: 'item', foreignKey: 'itemId'}]});
		}
		else {
			return Promise.reject({reason: 'loggedOut', message: "User is logged out"});
		}
	},
	register: function (req, userName) {
		return db.sequelize.transaction(function (transaction) {
			return db.User.create({
				userName: userName,
				coins: constants.defaultCoins
			}, {transaction: transaction}).then(function (user) {
				req.session.currentUser = user;
				return user;
			}).then(function (user) {
				return db.Item.bulkCreate([
					{
						name: "Bread",
						image: constants.breadImage
					}, {
						name: "Carrots",
						image: constants.carrotImage
					}, {
						name: "Diamonds",
						image: constants.diamondImage
					}
				], {returning: true, transaction: transaction});

			}).then(function (items) {
				var defaultQuantityMap = {
					Bread: constants.defaultBreads,
					Carrots: constants.defaultCarrots,
					Diamonds: constants.defaultDiamonds
				};
				return db.UserItem.bulkCreate(items.map(function (item) {
					return {
						quantity: defaultQuantityMap[item.name],
						userId: req.session.currentUser.id,
						itemId: item.id
					}
				}), {transaction: transaction});
			});
		}).then(function(){
			return req.session.currentUser;
		}).catch(function(error){
			console.error('error', error);
			return {reason: 'internalError', message: JSON.stringify(error)};
		});
	},
	login: function (req, userName) {
		return db.User.findOne({where: {userName: userName}}).then(function (user) {
			if(user) {
				req.session.currentUser = user;
				return user;
			}
			else{
				return Promise.reject({reason: 'userDoesNotExist', message: "The user that tried to login does not exist"})
			}
		});
	},
	logout: function (req) {
		req.session.currentUser = undefined;
		return Promise.resolve({success: true});
	}
};

module.exports = currentUserApi;
