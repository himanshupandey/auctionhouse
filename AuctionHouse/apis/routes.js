"use strict";

var express = require('express');
var Promise = require('promise');
var router = express.Router();
var currentUserApi = getAuctionHouseModule('AuctionHouse/apis/currentUser');
var currentAuction = getAuctionHouseModule('AuctionHouse/apis/currentAuction');

/* GET home page. */
router.get('/current-user', function (req, res, next) {
	currentUserApi.getCurrentUser(req).then(function (obj) {
		res.send(obj);
	}).catch(function (error) {
		res.status(error.message?400: 500);
		res.send({
			status: error.message?400: 500,
			reason: (error || {}).reason || "internalError",
			message: error.message? error.message : JSON.stringify(error)
		});
	});
});

/* GET home page. */
router.get('/current-user-items', function (req, res, next) {
	currentUserApi.getCurrentItems(req).then(function (obj) {
		res.send(obj);
	}).catch(function (error) {
		res.status(error.message?400: 500);
		res.send({
			status: error.message?400: 500,
			reason: (error || {}).reason || "internalError",
			message: error.message? error.message : JSON.stringify(error)
		});
	});
});
router.post('/login-or-register', function (req, res, next) {
	if (req && req.body && req.body.userName) {
		currentUserApi.login(req, req.body.userName).then(function (obj) {
			res.send(obj);
		}).catch(function (obj) {
			console.log('login error', JSON.stringify(obj));
			currentUserApi.register(req, req.body.userName).then(function (user) {
				res.send(user);
			}).catch(function (error) {
				res.status(error.reason?400:500);
				res.send({status: error.reason?400:500, reason: (error || {}).reason || "internalError", message: JSON.stringify(error)})
			});
		});
	}
	else {
		res.status(400);
		res.send({status: 400, reason: "userNameRequired", message: "User name required"})
	}
});

router.get('/current-auction', function (req, res, next) {
	currentAuction.getCurrentAuction({app: req.app}).then(function (auction) {
		res.send(auction);
	}).catch(function (error) {
		console.error(error);
		res.status(error.reason?400:500);
		res.send({status: error.reason?400:500, reason: (error || {}).reason || "internalError", message: JSON.stringify(error)})
	});
});

router.post('/start-auction', function (req, res, next) {
	currentAuction.startAuction(req, req.body).then(function (auction) {
		res.send(auction);
	}).catch(function (error) {
		res.status(error.reason?400:500);
		console.error(error);
		res.send(error);
	});
});
router.post('/place-bid', function (req, res, next) {
	currentAuction.placeBid(req, req.body).then(function (auction) {
		res.send(auction);
	}).catch(function (error) {
		res.status(error.reason?400:500);
		res.send({status: error.reason?400:500, reason: (error || {}).reason || "noActiveAuction", message: JSON.stringify(error)})
	});
});

module.exports = router;
