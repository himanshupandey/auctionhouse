(function (exports) {
    "use strict";
    var assetHashes = (getAuctionHouseModule('asset_hash')).assetHashes,
        asset_helper = (getAuctionHouseModule('asset_helper')).asset_helper(assetHashes),
        fs = require('fs'),
        path = require('path');
    exports.postfixUrlWithHash = function (grunt) {
        return function () {
            var done = this.async(),
                stylePath = path.resolve(__basedir, 'public/static/css/style.css'),
                encoding = 'utf8';
            fs.readFile(stylePath, encoding, function (err, data) {
                if (!err) {
                    var urlRegex = /url[\s]*?\(\s*?[\'\"]((?!data).)*?(assets[\S].*?)[\"\']\s*?\)/g;
                    data = data.replace(urlRegex, function (matchString, subGroupMatch1, subGroupMatch2) {
                        return ("url('../.." + asset_helper.asset(subGroupMatch2, true) + "')");
                    });
                    fs.writeFile(stylePath, data, encoding, function(err){
                        if(err) {
                            throw err;
                        }
                        else{
                            done();
                        }
                    });
                }
            });

        };
    };
})(exports);
