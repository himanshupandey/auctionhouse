(function(exports){
    "use strict";
    var fs = require('fs'),
        path = require('path'),
        upath = require('upath'),
        walk = require('fs-walk'),
        stableStringify = require('json-stable-stringify'),
        encoding = 'utf8',
        publicDirectory = '../parse/public',
        fileHashes = {};

    exports.generateAssetHash = function(grunt) {
        return function () {
        var done = this.async();
            function errorHandler(err, next) {
                if (typeof next === "function" && err) {
                    grunt.log.error(err);
                    next(err);
                    done();
                }
                return false;
            }

            walk.files(publicDirectory, function (basedir, filename, stat, next) {
                fs.readFile(path.join(basedir, filename), encoding, function (err, fileData) {
                    if (!errorHandler(err)) {
                        var relativePath = upath.normalize(path.relative(publicDirectory, path.join(basedir, filename)));
                        fileHashes[relativePath] = hasher(fileData) ;
                        next();
                    }
                });
            }, function () {
                var fileContent = "exports.assetHashes = " + stableStringify(fileHashes, {space: '\t'}) + ";";
                fs.writeFile('../parse/cloud/asset_hash.js', fileContent, encoding, function (err) {
                    if (!errorHandler(err)) done();
                })
            });
        };
    };
    function hasher(data) {
        var hash = 5381;
        for (var dataIndex = data.length; dataIndex ; dataIndex--) {
            hash = ((hash << 5) + hash) + data.charCodeAt(dataIndex - 1);
            /* hash * 33 + c */
        }
        return hash;
    }
})(exports);
