"use strict";

var Sequelize = require("sequelize");
var Item = getAuctionHouseModule('models/item');
var User = getAuctionHouseModule('models/user');
module.exports = function(sequelize, DataTypes) {
  var Auction = sequelize.define("Auction", {
		quantity: DataTypes.INTEGER,
    minBid: DataTypes.INTEGER,
		createdAt: {
			type: DataTypes.DATE
		},
		updatedAt: {
			type: DataTypes.DATE
		},
    completed: DataTypes.BOOLEAN
  }, {
		freezeTableName: true,

		// define the table's name
		tableName: 'Auction',

    classMethods: {
      associate: function(models) {
        models.Auction.belongsTo(models.Item, {as: 'item', foreignKey: 'itemId'});
        models.Auction.belongsTo(models.User, {as: 'user', foreignKey: 'userId'});
      }
    }
	});
  return Auction;
};
