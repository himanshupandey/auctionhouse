"use strict";

module.exports = function(sequelize, DataTypes) {
  var Item = sequelize.define("Item", {
    name: DataTypes.STRING,
    image: DataTypes.STRING,
		createdAt: {
			type: DataTypes.DATE
		},
		updatedAt: {
			type: DataTypes.DATE
		}
  }, {
		freezeTableName: true,

		// define the table's name
		tableName: 'Item',
		//
		// classMethods: {
		// 	associate: function (models) {
		// 		Item.belongsToMany(models.User, {through: models.UserItem});
		// 	}
		// }
	});
  return Item;
};
