"use strict";

var Sequelize = require("sequelize");
var Item = getAuctionHouseModule('models/item');
var User = getAuctionHouseModule('models/user');
module.exports = function (sequelize, DataTypes) {
	var UserItem = sequelize.define("UserItem", {
		quantity: DataTypes.INTEGER,
		// itemId: {
		// 	type: DataTypes.INTEGER,
		//
		// 	references: {
		// 		// This is a reference to another model
		// 		model: Item,
		//
		// 		// This is the column name of the referenced model
		// 		key: 'id',
		//
		// 		// This declares when to check the foreign key constraint. PostgreSQL only.
		// 		deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
		// 	}
		// },
		// userId: {
		// 	type: Sequelize.INTEGER,
		//
		// 	references: {
		// 		// This is a reference to another model
		// 		model: User,
		//
		// 		// This is the column name of the referenced model
		// 		key: 'id',
		//
		// 		// This declares when to check the foreign key constraint. PostgreSQL only.
		// 		deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
		// 	}
		// },
		createdAt: {
			type: DataTypes.DATE
		},
		updatedAt: {
			type: DataTypes.DATE
		}
	}, {
		freezeTableName: true,

		// define the table's name
		tableName: 'UserItem',

    classMethods: {
      associate: function(models) {
        models.UserItem.belongsTo(models.Item, {as: 'item', foreignKey: 'itemId'});
        models.UserItem.belongsTo(models.User, {as: 'user', foreignKey: 'userId'});
      }
    }
	});
	return UserItem;
};
