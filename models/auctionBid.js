"use strict";

var Sequelize = require("sequelize");

var User = getAuctionHouseModule('models/user');
var Auction = getAuctionHouseModule('models/auction');
module.exports = function (sequelize, DataTypes) {
	var AuctionBid = sequelize.define("AuctionBid", {
		value: DataTypes.INTEGER,
		createdAt: {
			type: DataTypes.DATE
		},
		updatedAt: {
			type: DataTypes.DATE
		}
	}, {
		indexes: [
			{
				unique: true,
				fields: ['auctionId', 'bidderId']
			}
		],
		freezeTableName: true,

		// define the table's name
		tableName: 'AuctionBid',

		classMethods: {
			associate: function (models) {
				models.AuctionBid.belongsTo(models.Auction, {as: 'auction', foreignKey: 'auctionId'});
				models.AuctionBid.belongsTo(models.User, {as: 'bidder', foreignKey: 'bidderId'});
			}
		}
	});
	return AuctionBid;
};
