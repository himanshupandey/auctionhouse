"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require("config");
var env = config.get('PRODUCTION') ? 'production' : 'development';
var sequelize = new Sequelize(config.get("databaseSetting.name"),
	config.get("databaseSetting.user"), config.get("databaseSetting.password"), {
		host: config.get("databaseSetting.host"),
		dialect: config.get("databaseSetting.dialect"),
		pool: {
			max: 5,
			min: 0,
			idle: 10000
		}
	});
var db = {};

fs
	.readdirSync(__dirname)
	.filter(function (file) {
		return (file.indexOf(".") !== 0) && (file !== "index.js");
	})
	.forEach(function (file) {
		var model = sequelize.import(path.join(__dirname, file));
		db[model.name] = model;
	});

Object.keys(db).forEach(function (modelName) {
	if ("associate" in db[modelName]) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
