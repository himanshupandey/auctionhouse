"use strict";

module.exports = function (sequelize, DataTypes) {
	var User = sequelize.define("User", {
		userName: {
			type: DataTypes.STRING,
			unique: true
		},
		createdAt: {
			type: DataTypes.DATE
		},
		updatedAt: {
			type: DataTypes.DATE
		},
		coins: DataTypes.INTEGER
	}, {
		freezeTableName: true,

		// define the table's name
		tableName: 'User',
		//
    // classMethods: {
    //   associate: function(models) {
    //     // User.hasMany(models.Item, { as: 'items', through: 'UserItem' });
    //   }
    // }
	});
	return User;
};
