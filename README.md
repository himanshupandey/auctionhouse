# Auction House

## Steps to setup

### i) Setup NVM, Node, NPM

##### 1- Install nvm v0.29.0

As described at [link](https://github.com/creationix/nvm) run following command to install nvm

```sh
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash
```
If you are using windows you need to used alternative of nvm  
a) [nvmw](https://github.com/hakobera/nvmw)  
b) [nvm-windows](https://github.com/coreybutler/nvm-windows)  
After installing nvm you need to start a new terminal for nvm to be recognized. Or reset source to .bashrc.  

##### 2- Install Node v6.3.1
To install node run command
```sh
$ nvm install v6.3.1
```
To use node v6.3.1 run command
```sh
$ nvm use v6.3.1
```

### ii) Setup project

#### 1- Clone repository using command
```sh
$ git clone git@bitbucket.org:himanshupandey/auctionhouse.git
```

#### 2- Install local packages and grunt interface

##### a)- Install grunt command interface
Run following command to install grunt command interface
```sh
$ npm install -g grunt-cli
```
##### b) Move to project folder in project
```sh
$ cd /path_to_AuctionHouse/
```
##### c) Run command to install local packages
```sh
$ npm install
```
##### b) Move to assets folder in project
```sh
$ cd /path_to_AuctionHouse/AuctionHouse/public/assets
```
##### d) Run command to install bower packages
```sh
$ bower install
```
##### d) Update configuration files
Update config/defaule.json with all keys and secret data in the file for your app.

# Run server locally for debugging purposes

#### 1- Run local server
Run one following commands to run server

```npm start```

#### 3- open server in browser
By default it will run server on port 3000. Hit url ```http://localhost:3000/``` to access server

# Run test cases
#### To run all test cases
Move to parse folder and run command  
```sh
$ npm test
```

# Setup Redis
#### Installation
```sh
$ wget http://download.redis.io/releases/redis-3.2.1.tar.gz
```
```sh
$ tar xzf redis-3.2.1.tar.gz
```
```sh
$ cd redis-3.2.1
```
```sh
$ make
```


# Setup Database
#### Installation
Follow instructions at link https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-14-04
And default json and config.json


# Grunt Tasks
#### 1- Default task

By default grunt would run '**less**' and '**concat**' tasks. '**less**' task would compiled less file spedified in file '**Gruntfile.js**' to generate '**style.less**'. '**concat**' combines all js file specified in '**Gruntfile.js**' into one application.js file

Run following command for default task
```sh
$ grunt
```
#### 2- Prod task

By default grunt would run '**less**' and '**uglify**' tasks. '**less**' task would compiled and minify all less file spedified in file '**Gruntfile.js**' to generate '**style.less**'. '**uglify**' combines and minify all js file specified in '**Gruntfile.js**' into one application.js file

Run following command for default task
```sh
$ grunt prod
```

#### 3- Watch task

Watch basically monitors all js and less file. It detects
changes in files and run specified task on it.
It would run less, concat, hashify, and hashify_style_url for every change in js and less file.
Run following command for watch task
```sh
$ grunt watch
```
#### 4- Jshint task
Task to check warnings and errors in js file in our application.
Run following command for jshint task
```sh
$ grunt jshint
```
#### 5- Less task
Compile less files to generate one style.css file

Run following command for less task
```sh
$ grunt less
```
#### 6- Uglify task
Compile and compress js files to generate one application js file

Run following command for uglify task
```sh
$ grunt uglify
```

#### 7- Concat task
Concats all js file in one single application js file. It is faster than uglify and code is readable. So it should be prefered for development and debugging purposes.
```sh
$ grunt concat
```

#### 8- Hashify assets task
Generate hash all file in public folder and store it in asset_hash.js file in cloud folder. It solves the problem of browser caching.
```sh
$ grunt hashify
```

#### 9- Hashify css urls task
replace all urls in style.css in static/css folder to have hash parameter at the end. It helps in solving the problem of browser caching.
```sh
$ grunt hashify_style_url
```
